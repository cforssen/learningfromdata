# Learning from data
This repository contains the lecture notes, notebooks and scripts for the course "Learning from data".
Author: CHristan Forssén, Department of Physics, Chalmers University of Technology.

The JupyterBook can be accessed at: https://cforssen.gitlab.io/learningfromdata/
